# IGWN - Low Latency Data Distribution

The IGWN - Low Latency Data Distribution (lldd) is software to distribute low latency data used by the International Gravitational-Wave Observatory Network (IGWN). 

This software currently utilizes the [Apache Kafka](https://kafka.apache.org/) event streaming platform to distribute the low latency data by connecting to Kafka Broker(s) to produce or consume Kafka messages containing the data. 

